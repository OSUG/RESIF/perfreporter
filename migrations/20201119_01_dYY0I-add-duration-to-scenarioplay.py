"""
Add duration to scenarioplay
"""

from yoyo import step

__depends__ = {'20201106_04_PFY8D-alter-performances'}

steps = [
    step("ALTER TABLE scenarioplays ADD COLUMN duration BIGINT","ALTER TABLE scenarioplays DROP COLUMN duration")
]
