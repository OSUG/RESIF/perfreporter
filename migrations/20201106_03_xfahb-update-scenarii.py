"""
Update scenarii
"""

from yoyo import step

__depends__ = {'20201106_02_WF2gB-add-pythonclass-to-scenarii-table'}

steps = [
    step("UPDATE scenarii set pythonclass = 'Scenario1' WHERE description ='30 requêtes query séquentielles de 60 secondes'"),
    step("UPDATE scenarii set pythonclass = 'Scenario2' WHERE description ='30 requêtes queryauth séquentielles de 60 secondes'"),
    step("UPDATE scenarii set pythonclass = 'Scenario3' WHERE description ='30 requêtes query parallélisées par 6 de 60 secondes'"),
]
