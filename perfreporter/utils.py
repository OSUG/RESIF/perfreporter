#!/usr/bin/env python3

from datetime import timedelta
import random

def random_date(start, end):
    """
    This function will return a random datetime between two datetime
    objects.
    If end < start, returns start
    """
    if end < start:
        return start
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = random.randint(1, int_delta)
    return start + timedelta(seconds=random_second)
