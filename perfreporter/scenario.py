#!/usr/bin/env python3
import logging
import random
from datetime import datetime, timedelta
import requests
import multiprocessing as mp
from dataselectperf import DataselectPerf
import time
# TODO Add progress par

from settings import Settings
import utils

class Scenario():
    """
    Un schenario défini les tests à réaliser
    """
    channels = requests.get(f"{Settings.baseurl}/fdsnws/station/1/query?level=channel&format=text", headers=Settings.headers)

    def __init__(self, sid):
        self.scenario_id = sid
        self.date = datetime.now()
        self.performances = []
        self.duration = 0
        # Not very robust. TODO : do something better
        self.channels_list = self.channels.text.split("\n")

    def __repr__(self):
        return f" {self.scenario_id} | {self.date.strftime(Settings.timeformat)}"

    def set_channels_list(self):
        """
        Prépare une liste de channels pour ce scénario
        """

    def play(self):
        logging.info("Starting Scenario %s", self.scenario_id)
        if len(self.channels_list) == 0:
            self.set_channels_list()
        tic = time.perf_counter()
        self.perform()

        toc = time.perf_counter()
        self.duration = int((toc - tic)*1000)

    def get_channel_ref(self, linenum):
        """
        Choisi au hasard un canal et un temps de début.
        channels : le texte de sortie de la requête station
        Renvoie un nupplet qui convienne pour la fonction get_data
        """
        ref_line = self.channels_list[linenum]
        channel_line = ref_line.split('|')
        start = datetime.strptime(channel_line[-2], Settings.timeformat)
        end = datetime.strptime(channel_line[-1], Settings.timeformat)
        delta = timedelta(seconds=60)
        random_start = utils.random_date(start, end - delta)
        return (channel_line[0], channel_line[1], channel_line[2], channel_line[3], random_start)

    def perform(self):
        """
        Chaque scenarion défini sont protocole de test et l'implémente dans cette fonction
        """

class Scenario1(Scenario):
    """
    30 Requêtes séquentielles de 1 minute de données aléatoires en query
    """
    def perform(self):
        loop = 30
        self.performances = []
        while loop > 0:
            r = random.randint(1,len(self.channels_list)-2)
            cha_refs = self.get_channel_ref(r)
            perf = DataselectPerf(*cha_refs, 60)
            perf.get_data(method="query")
            if perf.response == 200:
                loop = loop -1
                self.performances.append(perf)


class Scenario2(Scenario):
    """
    30 requêtes queryauth séquentielles de 60 secondes
    """
    def perform(self):
        loop = 30
        while loop > 0:
            r = random.randint(1,len(self.channels_list)-2)
            logging.debug(self.channels_list[r])
            cha_refs = self.get_channel_ref(r)
            perf = DataselectPerf(*cha_refs, 60)
            perf.get_data("queryauth")
            # Si on a un 401, pas la peine de s'acharner
            if perf.response == 401:
                logging.error("Authentication failed for queryauth")
                return False
            if perf.response == 200:
                loop = loop -1
                self.performances.append(perf)

class Scenario3(Scenario):
    """
    30 requêtes query parallélisées par 10 pour récupérer 60 secondes
    """
    def play_one(self):
        logging.info("Starting job")
        if len(self.channels_list) == 0:
            self.set_channels_list()
        loop = 10
        self.performances = []
        while loop > 0:
            r = random.randint(1,len(self.channels_list)-2)
            cha_refs = self.get_channel_ref(r)
            perf = DataselectPerf(*cha_refs, 60)
            perf.get_data(method="query")
            if perf.response == 200:
                loop = loop -1
                self.performances.append(perf)

    def perform(self):
        output = mp.Queue()
        processes = [mp.Process(target=self.play_one()) for x in range(3)]
        i = 0
        for p in processes:
            logging.info("Launching parallel process %d", i)
            p.start()
            i = i+1
        for p in processes:
            p.join()
