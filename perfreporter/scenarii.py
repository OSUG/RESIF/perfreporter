#!/usr/bin/env python3
#
import psycopg2
import importlib
import logging
from settings import Settings

class Scenarii():
    """Gestion des scenarii"""
    def __init__(self, comment):
        self.scenarii_list = [] # Liste des scenarios définis dans la table scenarii
        self.scenarii = []      # Liste des instances de Scenario
        self.comment = comment

    def get_from_db(self):
        try:
            scenario_module = importlib.import_module("scenario")
        except Exception as err:
            logging.error(err)

        with psycopg2.connect(Settings.dburi) as conn:
            with conn.cursor() as curs:
                curs.execute("SELECT id, description, pythonclass from scenarii")
                self.scenarii_list = curs.fetchall()
                for sc in self.scenarii_list:
                    logging.debug("Instanciating a %s class", sc[2])
                    ScenarioClass = getattr(scenario_module, sc[2])
                    self.scenarii.append(ScenarioClass(sc[0]))
                # TODO Documenter l'ajout d'un scénario

    def register_to_db(self):
        insert_scenario_request = "INSERT INTO scenarioplays (scenario_id, comment, date, duration) VALUES (%s, %s, %s, %s) RETURNING id"
        # TODO Add start and end in performance table
        insert_performance_request = "INSERT INTO performances (scenarioplay_id, network, station, location, channel, bytes, millisecs) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        with psycopg2.connect(Settings.dburi) as conn:
            with conn.cursor() as curs:
                for sc in self.scenarii:
                    if len(sc.performances) > 0:
                        logging.info("Inserting %s performances", len(sc.performances))
                        curs.execute(insert_scenario_request, (sc.scenario_id, self.comment, sc.date, sc.duration))
                        sc_id = curs.fetchone()[0]
                        for p in sc.performances:
                            curs.execute(insert_performance_request, (sc_id, p.net, p.sta, p.loc, p.cha, p.size, p.perf ))

    def __repr__(self):
        rs = " id | description             | pythonclass\n"
        for s in self.scenarii_list:
            rs = rs + f"  {s[0]} | {s[1]} | {s[2]} \n"
        return rs
