"""
alter performances
"""

from yoyo import step

__depends__ = {'20201106_03_xfahb-update-scenarii'}

steps = [
    step("ALTER TABLE performances ALTER COLUMN millisecs TYPE DECIMAL", "ALTER TABLE performances ALTER COLUMN millisecs TYPE INTEGER")
]
