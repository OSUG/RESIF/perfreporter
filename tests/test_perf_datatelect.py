#!/usr/bin/env python3

import pytest
from perfreporter.perf_dataselect import Scenarii, Scenario

class TestScenarii:
    """
    Test de la class Scenarii
    """
    def test_constructor(self):
        scs = Scenarii("postgresql://performances@resif-pgprod.u-ga.fr/performances")
        scs.get_from_db()
