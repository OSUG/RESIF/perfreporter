
"""
Ce script prend au hasard 60 secondes de données parmis toutes les stations de RESIF.

"""
import logging
import click
from settings import Settings
from scenarii import Scenarii

logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

@click.group()
@click.option('--verbose', '-v', is_flag=True, help="Will print verbose messages.")
@click.option('--db-uri', '-d', envvar='DBURI', help="Database URI (postgresql://user:pass@host/database)" )
@click.option('--auth-pass', envvar='AUTHPASS', help="Database URI (postgresql://user:pass@host/database)" )
@click.option('--auth-user', envvar='AUTHUSER', help="Database URI (postgresql://user:pass@host/database)" )
@click.option('--base-url', '-b', envvar='BASEURL',  help="Database URI (postgresql://user:pass@host/database)", default="https://ws.resif.fr" )
def cli(verbose, db_uri, auth_pass, auth_user, base_url):
    Settings.baseurl = base_url
    Settings.verbose = verbose
    Settings.dburi = db_uri
    Settings.auth_user = auth_user
    Settings.auth_pass = auth_pass

@cli.command()
@click.option('--allsc', '-a', is_flag=True, help="Play all scenarii")
@click.option('--message', '-m', type=str, help="Comment for this play", required=True)
@click.option('--dryrun', '-n', is_flag=True, help="Do not register in database")
@click.argument('scenario_ids', nargs=-1, type=int)
def play(scenario_ids, allsc, message, dryrun):
    """
    Play one or several scenarii
    """
    # Get all scenarios
    scenarii = Scenarii(message)
    scenarii.get_from_db()
    for scenario in scenarii.scenarii:
        if allsc or scenario.scenario_id in scenario_ids:
            logging.info("Playing scenario %s", scenario.scenario_id)
            scenario.play()
        else:
            logging.debug("Not playing scenario %s", scenario.scenario_id)
            scenarii.scenarii.remove(scenario)
    if not dryrun:
        logging.info("Registering to database")
        scenarii.register_to_db()
    return 0

@cli.command()
def list():
    """
    List all scenarii stored in database
    """
    scenarii = Scenarii("List")
    scenarii.get_from_db()
    print(scenarii)

if __name__ == '__main__':
    cli()
