#!/usr/bin/env python3

from datetime import datetime, timedelta
import time
import logging
import requests
from requests.auth import HTTPDigestAuth

from settings import Settings

class DataselectPerf():
    """
    Classe pour gérer un resultat de performance
    """
    def __init__(self, net, sta, loc, cha, start, seconds):
        self.date = datetime.now()
        self.perf = 0
        self.size = 0
        self.response = 0
        self.net = net
        self.sta = sta
        self.loc = loc
        self.cha = cha
        self.start = start
        self.end = self.start + timedelta(seconds=seconds)
        self.method = ""

    def get_data(self, method):
        """
        Télécharge 60 secondes de la donnée demandée
        Renvoie le temps mis pour réaliser l'opération
        method peut valoir "query" ou "queryauth"
        """
        url = f"{Settings.baseurl}/fdsnws/dataselect/1/{method}?net={self.net}&sta={self.sta}&loc={self.loc}&cha={self.cha}&start={self.start.strftime(Settings.timeformat)}&end={self.end.strftime(Settings.timeformat)}"
        logging.debug(url)
        if method == "queryauth":
            tic = time.perf_counter()
            data = requests.get(url, headers=Settings.headers, auth=HTTPDigestAuth(Settings.auth_user, Settings.auth_pass))
            toc = time.perf_counter()
        else:
            tic = time.perf_counter()
            data = requests.get(url, headers=Settings.headers)
            toc = time.perf_counter()

        self.size = len(data.content)
        self.perf = int((toc - tic)*1000)
        self.response = data.status_code
        self.method = f"dataselect/1/{method}"


    def __str__(self):
        return f"{self.size} bytes, {self.perf} milliseconds, {int(self.size / self.perf)}bytes/sec, rc {self.response}"
