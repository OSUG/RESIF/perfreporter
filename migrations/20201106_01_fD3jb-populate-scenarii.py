"""
Populate scenarii
"""

from yoyo import step

__depends__ = {'20201105_01_i1vdR-create-tables'}

steps = [
    step("INSERT INTO scenarii (description) VALUES ('30 requêtes query séquentielles de 60 secondes')"),
    step("INSERT INTO scenarii (description) VALUES ('30 requêtes queryauth séquentielles de 60 secondes')"),
    step("INSERT INTO scenarii (description) VALUES ('30 requêtes query parallélisées par 6 de 60 secondes')"),
]
