"""
Create tables
"""

from yoyo import step

steps = [
    step(
        "CREATE TABLE scenarii (id SERIAL, description TEXT, PRIMARY KEY(id))",
        "DROP TABLE scenarii"
    ),
    step(
        "CREATE TABLE scenarioplays (id SERIAL, scenario_id INT REFERENCES scenarii(id), date TIMESTAMP, comment TEXT, PRIMARY KEY(id))",
        "DROP TABLE scenarioplays"
    ),
    step("CREATE TABLE performances (id SERIAL, scenarioplay_id INT REFERENCES scenarioplays(id), network VARCHAR(2), station VARCHAR(5), location VARCHAR(2), channel VARCHAR(3), bytes INT, millisecs INT, PRIMARY KEY(id))",
        "DROP TABLE performances")

    ]
