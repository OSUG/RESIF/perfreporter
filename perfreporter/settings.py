#!/usr/bin/env python3

"""
Usefull global variables
"""

class Settings:
    """
    Classe pour la configuration
    Il n'y a pas de self, alors les variables sont partagées par toutes les instances de la classe
    Cette classe existe, même si elle n'est pas instanciée.
    On peut donc faire:
    import globals as gl
    gl.settings.timeformat

    """
    timeformat = '%Y-%m-%dT%H:%M:%S'
    headers = { 'User-Agent': 'Résif Perf tester' }
    baseurl = "https://ws.resif.fr"
    verbose = False
    dburi = "postgresql://performances@localhost/performances"
    auth_user = "resifdc"
    auth_pass = "plop"

    def __repr__(self):
        return f"=== Settings ===\ntimeformat: {self.timeformat}\nheaders: {self.headers}\nbaseurl: {self.baseurl}\nverbose: {self.verbose}\ndburi: {self.dburi}"
