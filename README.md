# Tests de performance dataselect

## Utilisation

    python perfreporter/perf_dataselect.py -d postgresql://performances@resif-pgprod.u-ga.fr/performances list

## Script de génération des requêtes

Configuré par variables d'environnements :

    - `AUTHUSER` : utilisateur pour les requêtes queryauth (par défaut : `resifdc`) 
    - `AUTHPASS` : mot de passe pour les requêtes queryauth (par défaut : `plop`) 
    - `BASEURL` : url de base pour les webservices (par défaut : `ws.resif.fr`)
    - `DBURI` : URI pour se connecter à une base de données (par exemple `postgresql://perfer:PASSWORD@resif-pgprod.u-ga.fr/performances`)

Exemple :

    AUTHPASS=plop DBURI=postgresql://performances@resif-pgprod.u-ga.fr:5432/performances python perf_dataselect.py list
    AUTHPASS=plop DBURI=postgresql://performances@resif-pgprod.u-ga.fr:5432/performances python perf_dataselect.py play 2 4
    AUTHPASS=plop DBURI=postgresql://performances@resif-pgprod.u-ga.fr:5432/performances python perf_dataselect.py play --allsc

## Base de données

Dans une base de donnée, il faut les tables suivantes:

- scenarii : la liste des scénarios définis, avec description et nom de la classe Python qui implémente ce scénario
- scenario_plays : l'historique des scénarios joués, avec identifiant du scénario, date et commentaire
- performances : la table des performances, chacune étant associée à un scénario

La création et migration des tables est faite avec l'outil `yoyo` (https://ollycope.com/software/yoyo/latest/).

Prérequis : créer une base de données et un utilisateur ayant tous les droits dessus.

Puis appliquer les migrations par exemple :

  yoyo migrate --database postgresql://performances@resif-pgprod.u-ga.fr:5432/performances

Cela met en place le schéma initial

## Concepts hyper sioux

On défini des scénarios pour tester les performances.

Ils sont définis dans `scenario.py` et héritent de la classe `Scenario` et définissent une méthode `play()`.

Cette méthode doit instancier des objets `DataselectPerf` et les référencer.

Tout cela est géré par un objet `Scenarii` qui gère une liste de scénarios.

Pour définir un nouveau scénario il faut :

  - définir une classe (par exemple `ScenarioNew`) et définissant sa méthode `play()`
  - ajouter une référence à ce scénario dans la table SQL `scenarii` : `INSERT INTO scenarii (description, pythonclass) VALUES (Scenario incroyable', 'ScenarioNew')`

